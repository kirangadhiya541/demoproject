import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { ScannDocumentPage } from './scanndocument.page';

describe('ScannDocumentPage', () => {
  let component: ScannDocumentPage;
  let fixture: ComponentFixture<ScannDocumentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScannDocumentPage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ScannDocumentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
