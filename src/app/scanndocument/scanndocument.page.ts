import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scanndocument',
  templateUrl: 'scanndocument.page.html',
  styleUrls: ['scanndocument.page.scss']
})
export class ScannDocumentPage {
  public checklist: any = [
    {
      step: 1,
      title: 'General Document',
      isopen: false
    },
    {
      step: 2,
      title: 'Income Receipt',
      isopen: false
    },
    {
      step: 3,
      title: 'Wealth',
      isopen: false
    },
    {
      step: 4,
      title: 'Debits',
      isopen: false
    },
  ]
  constructor(public router: Router) { }
  changePage(route) {
    this.router.navigateByUrl(route);
  }
  opencard(i){
    console.log("opencard");
    
    this.checklist[i].isopen =! this.checklist[i].isopen;
    console.log(this.checklist);
    
  }
}
