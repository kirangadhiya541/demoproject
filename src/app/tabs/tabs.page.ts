import { Component } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  public listtab: boolean = true;
  public calculatetab: boolean = false;
  public profiletab: boolean = false;
  public settingtab: boolean = false;
  constructor() { }
  activetab(key) {
    console.log("activetab");
    this.listtab = false;
    this.calculatetab = false;
    this.profiletab = false;
    this.settingtab = false;
    if (key == 'list') {
      this.listtab = true;
    }
    else if (key == 'cal') {
      this.calculatetab = true;
    }
    else if (key == 'setting') {
      this.settingtab = true;
    }
    else if (key == 'profile') {
      this.profiletab = true;
    }
  }
}
