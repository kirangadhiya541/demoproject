import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/tabs/tabs/tab1',
      icon: 'mail'
    },
    {
      title: 'Obersicht',
      url: '/tabs/tabs/tab1',
      icon: 'mail'
    },
    {
      title: 'Einreichen',
      url: '/tabs/tabs/tab1',
      icon: 'paper-plane'
    },
    {
      title: 'Ersparnis berechnen',
      url: '/tabs/tabs/tab1',
      icon: 'paper-plane'
    },
    {
      title: 'Checkliste',
      url: '/tabs/tabs/tab1',
      icon: 'paper-plane'
    },
    {
      title: 'Mein Profil',
      url: '/tabs/tabs/tab3',
      icon: 'paper-plane'
    },
    {
      title: 'Kontakt',
      url: '/tabs/tabs/tab1',
      icon: 'paper-plane'
    }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
