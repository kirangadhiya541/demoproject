import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: 'signup.page.html',
  styleUrls: ['signup.page.scss']
})
export class SignupPage {

  constructor(public route: Router) { }
  signup() {
    this.route.navigateByUrl('/tabs/tabs/tab1');
  }
}
