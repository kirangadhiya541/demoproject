import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checklist',
  templateUrl: 'checklist.page.html',
  styleUrls: ['checklist.page.scss']
})
export class CheckListPage {
  public checklist: any = [
    {
      step: 1,
      title: 'Unterlagen',
      isopen: false
    },
    {
      step: 2,
      title: 'Rund ums Enikmmoen',
      isopen: false
    },
    {
      step: 3,
      title: 'Rund ums and Enikmmoen',
      isopen: false
    }
  ]
  constructor(public router: Router) { }
  changePage(route) {
    this.router.navigateByUrl(route);
  }
  opencard(i){
    console.log("opencard");
    
    this.checklist[i].isopen =! this.checklist[i].isopen;
    console.log(this.checklist);
    
  }
}
